//
//  FirstVC.swift
//  WeatherTeam
//
//  Created by MacBook on 11.11.21.
//

import UIKit

class FirstVC: UIViewController {

    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var inputField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    @IBAction func inputAction(_ sender: Any) {
        outputLabel.text = inputField.text
        let secondVC = SecondVC(nibName: String(describing: SecondVC.self), bundle: nil)
        navigationController?.pushViewController(secondVC, animated: true)
    }
    
    

}
